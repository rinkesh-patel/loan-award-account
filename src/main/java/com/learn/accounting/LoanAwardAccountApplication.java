package com.learn.accounting;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class LoanAwardAccountApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoanAwardAccountApplication.class, args);
	}
	
    @PostConstruct
    void init(){
        TimeZone.setDefault(TimeZone.getTimeZone("America/Los_Angeles"));
    }

   /* @Bean
    public Jackson2ObjectMapperBuilderCustomizer jacksonObjectMapperCustomization() {
        return jacksonObjectMapperBuilder -> 
            jacksonObjectMapperBuilder.timeZone(TimeZone.getDefault());
    }*/
    
}
