package com.learn.accounting.domain;

import java.math.BigDecimal;
import java.util.function.Consumer;

public class LoanAwardAccountBuilder {

	public BigDecimal awardAmountInDollars;
	public Integer awardDurationInMonths;
	public BigDecimal yearlyInterestRate;

	public LoanAwardAccountBuilder with(Consumer<LoanAwardAccountBuilder> builder) {
		builder.accept(this);
		return this;
	}

	public LoanAwardAccount createLoanAwardAccount() {
		return new LoanAwardAccount(awardAmountInDollars, awardDurationInMonths, yearlyInterestRate);
	}

}
