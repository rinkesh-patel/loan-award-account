package com.learn.accounting.domain;

import java.math.BigDecimal;

public class LoanAwardAccount {
	
	private BigDecimal awardAmountInDollars;
	private Integer awardDurationInMonths;
	private BigDecimal yearlyInterestRate;
	
	public LoanAwardAccount() {
		
	}

	public LoanAwardAccount(BigDecimal awardAmountInDollars2, Integer awardDurationInMonths2,
			BigDecimal yearlyInterestRate2) {
		awardAmountInDollars=awardAmountInDollars2;
		awardDurationInMonths=awardDurationInMonths2;
		yearlyInterestRate=yearlyInterestRate2;
	}

	public BigDecimal getAwardAmountInDollars() {
		return awardAmountInDollars;
	}

	public Integer getAwardDurationInMonths() {
		return awardDurationInMonths;
	}
	
	public BigDecimal getYearlyInterestRate() {
		return yearlyInterestRate;
	}
}
