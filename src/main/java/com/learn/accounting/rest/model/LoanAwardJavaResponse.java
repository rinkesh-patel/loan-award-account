package com.learn.accounting.rest.model;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;

public class LoanAwardJavaResponse {

	private LocalDateTime localTime;
	private OffsetDateTime offsetTime;
	private Instant instantTime;
	private ZonedDateTime zonedTime;
	
	public LocalDateTime getLocalTime() {
		return localTime;
	}

	public void setLocalTime(LocalDateTime localTime) {
		this.localTime = localTime;
	}

	public OffsetDateTime getOffsetTime() {
		return offsetTime;
	}

	public void setOffsetTime(OffsetDateTime offsetTime) {
		this.offsetTime = offsetTime;
	}

	public Instant getInstantTime() {
		return instantTime;
	}

	public void setInstantTime(Instant instantTime) {
		this.instantTime = instantTime;
	}

	public ZonedDateTime getZonedTime() {
		return zonedTime;
	}

	public void setZonedTime(ZonedDateTime zonedTime) {
		this.zonedTime = zonedTime;
	}

	public LoanAwardJavaResponse() {
		
	}

}
