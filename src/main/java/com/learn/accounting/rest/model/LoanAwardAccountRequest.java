package com.learn.accounting.rest.model;

import java.math.BigDecimal;

public class LoanAwardAccountRequest {
	
	private BigDecimal awardAmountInDollars;
	private Integer awardDurationInMonths;
	private BigDecimal yearlyInterestRate;
	
	public LoanAwardAccountRequest() {
		
	}

	public BigDecimal getAwardAmountInDollars() {
		return awardAmountInDollars;
	}

	public void setAwardAmountInDollars(BigDecimal awardAmountInCents) {
		this.awardAmountInDollars = awardAmountInCents;
	}

	public Integer getAwardDurationInMonths() {
		return awardDurationInMonths;
	}

	public void setAwardDurationInMonths(Integer awardDurationInMonths) {
		this.awardDurationInMonths = awardDurationInMonths;
	}

	public BigDecimal getYearlyInterestRate() {
		return yearlyInterestRate;
	}

	public void setYearlyInterestRate(BigDecimal yearlyInterestRate) {
		this.yearlyInterestRate = yearlyInterestRate;
	}
	
}
