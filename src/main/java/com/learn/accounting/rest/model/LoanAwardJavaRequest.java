package com.learn.accounting.rest.model;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

public class LoanAwardJavaRequest {
	
	private OffsetDateTime offsetTimeInRequest;
	
	public LoanAwardJavaRequest() {
		
	}

	public OffsetDateTime getOffsetTimeInRequest() {
		return offsetTimeInRequest;
	}

	public void setOffsetTimeInRequest(OffsetDateTime offsetTimeInRequest) {
		this.offsetTimeInRequest = offsetTimeInRequest;
	}

	
}
