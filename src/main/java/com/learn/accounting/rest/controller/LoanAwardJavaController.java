package com.learn.accounting.rest.controller;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.TimeZone;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.learn.accounting.rest.model.LoanAwardJavaRequest;
import com.learn.accounting.rest.model.LoanAwardJavaResponse;

@RestController
@RequestMapping("/learn-api/v1/datetime")
public class LoanAwardJavaController {

	// https://medium.com/beingprofessional/think-functional-advanced-builder-pattern-using-lambda-284714b85ed5

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<LoanAwardJavaResponse> convertDateTime(
			@RequestBody final LoanAwardJavaRequest request) {

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.APPLICATION_JSON);
		LoanAwardJavaResponse response = new LoanAwardJavaResponse();
		
		System.out.println(request.getOffsetTimeInRequest());
		System.out.println(LocalDateTime.now());
		System.out.println(ZonedDateTime.now());

		OffsetDateTime offestTime = request.getOffsetTimeInRequest();
		LocalDateTime localTime = request.getOffsetTimeInRequest().toLocalDateTime();
		Instant instantTime = request.getOffsetTimeInRequest().toInstant();
		ZoneId zoneId = ZoneId.of( "America/Los_Angeles" );
		ZonedDateTime zonedTime = instantTime.atZone( zoneId );
//		ZonedDateTime zonedTime = request.getOffsetTimeInRequest().toZonedDateTime();
		
		response.setOffsetTime(offestTime);
		response.setLocalTime(localTime);
		response.setZonedTime(zonedTime);
		response.setInstantTime(instantTime);

		return new ResponseEntity<LoanAwardJavaResponse>(response, responseHeaders, HttpStatus.OK) {

		};
	}

}
