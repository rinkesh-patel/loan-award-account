package com.learn.accounting.rest.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.learn.accounting.domain.LoanAwardAccount;
import com.learn.accounting.domain.LoanAwardAccountBuilder;
import com.learn.accounting.rest.model.LoanAwardAccountRequest;
import com.learn.accounting.rest.model.LoanAwardAccountResponse;

@RestController
@RequestMapping("/customer-loan/v1/loan-award-accounts")
public class LoanAwardAccountAmortizationController {

	// https://medium.com/beingprofessional/think-functional-advanced-builder-pattern-using-lambda-284714b85ed5

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<LoanAwardAccountResponse> calculateAmortizationSchedule(
			@RequestBody final LoanAwardAccountRequest request) {

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.APPLICATION_JSON);

		return new ResponseEntity<LoanAwardAccountResponse>(prepareResponse(new LoanAwardAccountBuilder()
				.with($ -> {
					$.awardAmountInDollars=request.getAwardAmountInDollars();
					$.awardDurationInMonths=request.getAwardDurationInMonths();
					$.yearlyInterestRate=request.getYearlyInterestRate();
				}).createLoanAwardAccount()), responseHeaders, HttpStatus.OK) {

		};
	}

	private LoanAwardAccountResponse prepareResponse(final LoanAwardAccount awardAccount) {
		LoanAwardAccountResponse response = new LoanAwardAccountResponse();
		response.setAwardAmountInDollars(awardAccount.getAwardAmountInDollars());
		response.setAwardDurationInMonths(awardAccount.getAwardDurationInMonths());
		response.setYearlyInterestRate(awardAccount.getYearlyInterestRate());

		return response;

	}

}
